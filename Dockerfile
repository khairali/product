FROM openjdk:8
ARG envm
ADD /build/libs/demo-ps.jar app.jar
EXPOSE 8085
ENTRYPOINT ["java","-Dspring.profiles.active=a","-jar","app.jar"]
